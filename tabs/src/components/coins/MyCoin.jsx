import React, { useState } from "react";
import "./MyCoin.css";
import { useTeamsFx } from "./lib/useTeamsFx";
import { TeamsUserCredential } from "@microsoft/teamsfx";
import { useData } from "./lib/useData";

import { Button, Loader } from "@fluentui/react-northstar";
const axios = require("axios");

export function MyCoin() {
  const { isInTeams } = useTeamsFx();
  const userProfile = useData(async () => {
    const credential = new TeamsUserCredential();
    return isInTeams ? await credential.getUserInfo() : undefined;
  })?.data;
  const userName = userProfile ? userProfile.displayName : "";

  const [loading, setLoading] = useState(false);
  const [hasData, setHasData] = useState(false);
  const [remaining, setRemaining] = useState(0);
  const [total, setTotal] = useState(0);
  const [used, setUsed] = useState(0);

  async function getApi() {
    setLoading(true)
    const response = await axios.get(
      `https://api-qca4j.ondigitalocean.app/api/coins/12`
    );
    if (response.status === 200) {
      setRemaining(response.data.remaining)
      setTotal(+response.data.total)
      setUsed(response.data.used)
    }
    setHasData(true)
    setLoading(false)
  }

  return (
    <div className="welcome page">
      <div className="narrow page-padding">
        <h1 className="center">Coffe2meet</h1>
        <p className="center">Bienvenido {userName ? ", " + userName : ""}!</p>
        <p className="center">Haga clic en el botón de abajo para conocer sus fichas disponibles:</p>
        <Button primary className="center" content="Consultar fichas" disabled={loading} onClick={getApi} />
        {loading && (
          <pre className="fixed">
            <Loader />
          </pre>
        )}
        {!loading && hasData && (
          <div>
            <h3 className="center">Resultado:</h3>
            <p className="center"><strong>Restantes: </strong> {remaining}</p>
            <p className="center"><small><strong>Total: </strong> {total}</small></p>
            <p className="center"><small><strong>Usados: </strong> {used}</small></p>
          </div>
        )}
      </div>
    </div>
  );
}
